:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a
:set noswapfile
:let g:airline_theme='base16_gruvbox_dark_hard'
colorscheme gruvbox


call plug#begin()

	Plug 'tombh/novim-mode' "нормальная раскладка
	Plug 'vim-airline/vim-airline' "строка статуса снизу
	Plug 'ap/vim-css-color' "показывает цвет HEX #32a852
	Plug 'ryanoasis/vim-devicons' "доп. иконки в разных плагинах
	Plug 'terryma/vim-multiple-cursors'
	Plug 'scrooloose/syntastic' "поддержка синтаксиса
	Plug 'scrooloose/nerdcommenter' "поддержка комментирования
	Plug 'Vimjas/vim-python-pep8-indent'
	Plug 'neoclide/coc.nvim', {'branch': 'release'} "автодополнение
	Plug 'wincent/terminus' "терминал
	Plug 'vim-airline/vim-airline-themes'
	Plug 'scrooloose/nerdtree' "показ файлов/папок в виде дерева

call plug#end()


:map <C-c> <plug>NERDCommenterToggle
:map <F6> :NERDTreeToggle<CR>
